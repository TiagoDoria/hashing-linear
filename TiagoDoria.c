/*
Trabalho da disciplina Estrutura de Dados e Algoritmos II
Universidade Federal da Bahia
Autor: Tiago Dória

Implementação do algoritmo de hashing linear
*/


#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#define TAMANHO_ARQUIVO 11

typedef struct Registro{
	char nome[21];
    int chave, idade;	
	
}registro;

FILE *arquivo;
char Arquivo[] = "arquivo.bin";
float media2=0;
int med=0;
_Bool cheio = false;


// ------------------------------------INICIA ARQUIVO------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------

void IniciarArquivo()
{
	int i;
	registro r;
	
	r.chave = -1;
	arquivo = fopen(Arquivo,"ab");
	
	if(arquivo == NULL){
		printf("Falhar ao abrir arquivo!\n");
		return;
		}
	rewind(arquivo);	
	
	for(i=0; i<TAMANHO_ARQUIVO; i++){
		fseek(arquivo, ((i)*sizeof(registro)), SEEK_SET);	
		fwrite(&r, sizeof(registro), 1, arquivo); 
    }
    fclose(arquivo);	
}

// -----------------------------------------H1-------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

int h1(int h){
	return (h % TAMANHO_ARQUIVO);
}

// ----------------------------------------H2-------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------
 
int h2(int h){
	int valor = (h / TAMANHO_ARQUIVO);
	if(valor == 0)
		return 1;
	else
		return valor;
}

// ---------------------------------------H3---------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

int h3(int chave,int i){
	int hash1,hash2;
	
	hash1 = h1(chave);
	hash2 = h2(chave);
	int h3;
	h3 = (hash1 + i * hash2)%TAMANHO_ARQUIVO;
	return h3;
}

// --------------------------------------EXISTE------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

int existe(int chave){
		registro r;
		int salto,i=0;
		med = 0;
		
		int hashing = h3(chave,i);
		arquivo = fopen(Arquivo , "rb");
		rewind(arquivo);
		fseek(arquivo, ((hashing) * sizeof(registro)), SEEK_SET);
		fread(&r, sizeof(registro), 1,arquivo);
		fseek(arquivo, ((hashing) * sizeof(registro)), SEEK_SET);
		if(r.chave == chave){
			fclose(arquivo);
			med = 1;
			return 1;
			}
		else{
			while(r.chave != -1){
				i++;
				med++;
				salto = h3(chave,i);	
				rewind(arquivo);
				fseek(arquivo, ((salto) * sizeof(registro)), SEEK_SET);
				fread(&r, sizeof(registro), 1,arquivo);
				fseek(arquivo, ((salto) * sizeof(registro)), SEEK_SET);
				if(r.chave == chave){
					med++;
					fclose(arquivo);
					return 1;
									}			
			} // fim-while	
			
		} // fim-else
		
		fclose(arquivo);
		return 0;	
}

// -------------------------------------OVERFLOW-------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------- 

_Bool Overflow(){
	int i,c=0;
	
	registro r;
	c = 0;
	cheio = false;
	
	arquivo = fopen(Arquivo,"rb");
	rewind(arquivo);
	for(i=0;i<TAMANHO_ARQUIVO;i++){
		fseek(arquivo, ((i) * sizeof(registro)), SEEK_SET);
		fread(&r, sizeof(registro), 1,arquivo);
		fseek(arquivo, ((i) * sizeof(registro)), SEEK_SET);
		
		if(r.chave != -1)
			c++;	
		
		}
		
		if(c == TAMANHO_ARQUIVO)
			cheio = true;
	fclose(arquivo);
	return cheio;	
}

// -------------------------------------INSERE-------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------- 

void Insere(int chave, char nome[21], int idade){

    int hashing,i=0;
    registro r;
    hashing = h3(chave,i);
   
    if(chave == -1)
		return;
       
    if(Overflow())
		return;
		
    int x = existe(chave);
	if(x == 1){
		printf("chave ja existente: %d\n",chave);
		return;
	}

    arquivo = fopen(Arquivo, "rb+");
    if(arquivo == NULL){
		return;
	}	
	
	
	
	rewind(arquivo);
	fseek(arquivo, ((hashing) * sizeof(registro)), SEEK_SET);
	fread(&r, sizeof(registro), 1,arquivo);
	fseek(arquivo, ((hashing) * sizeof(registro)), SEEK_SET);
	if(r.chave == -1){
		r.chave = chave;
		strcpy(r.nome, nome);
		r.idade = idade;
		fwrite(&r, sizeof(registro), 1, arquivo);       
		fclose(arquivo);
		return;
		}
	else{
		int hash;
		while(r.chave != -1){
			i++;
			hash = h3(chave,i);
			rewind(arquivo);
			fseek(arquivo, ((hash) * sizeof(registro)), SEEK_SET);
			fread(&r, sizeof(registro), 1,arquivo);
			fseek(arquivo, ((hash) * sizeof(registro)), SEEK_SET);
			if(r.chave == -1){
				r.chave = chave;
				strcpy(r.nome, nome);
				r.idade = idade;
				fwrite(&r, sizeof(registro), 1, arquivo);       
				fclose(arquivo);
				return;
							}	
							}
		}
													
}

// -------------------------------------CONSULTA-----------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

int consulta(int chave,int media){
		registro r;
		int hash,i=0,c=0;
		
		 if(chave == -1)
			return 0;
		
		int hashing = h3(chave,i);
		arquivo = fopen(Arquivo,"rb");
		rewind(arquivo);
		fseek(arquivo, ((hashing) * sizeof(registro)), SEEK_SET);
		fread(&r, sizeof(registro), 1,arquivo);
		fseek(arquivo, ((hashing) * sizeof(registro)), SEEK_SET);
		if(r.chave == chave){
			printf("chave: %d\n", r.chave);
			printf("%s\n", r.nome);
			printf("%d\n", r.idade);
			fclose(arquivo);
			return 0;
			}
		else{
			while(r.chave != -1 && c < TAMANHO_ARQUIVO){
				i++;
				hash = h3(chave,i);	
				rewind(arquivo);
				fseek(arquivo, ((hash) * sizeof(registro)), SEEK_SET);
				fread(&r, sizeof(registro), 1,arquivo);
				fseek(arquivo, ((hash) * sizeof(registro)), SEEK_SET);
				if(r.chave == chave){
					printf("chave: %d\n", r.chave);
					printf("%s\n", r.nome);
					printf("%d\n", r.idade);
					fclose(arquivo);
					return 0;
									}							
				c++;		
						
			} // fim-while	
			
		} // fim-else
		
		printf("chave nao encontrada: %d\n",chave);
		fclose(arquivo);
		return 0;	
}

// ----------------------------------MEDIA INDIVIDUAL------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------
 
void MediaIndividual(int chave){
		registro r;
		int hash,i=0;
		med = 0;
		
		int hashing = h3(chave,i);
		arquivo = fopen(Arquivo , "rb");
		rewind(arquivo);
		fseek(arquivo, ((hashing) * sizeof(registro)), SEEK_SET);
		fread(&r, sizeof(registro), 1,arquivo);
		fseek(arquivo, ((hashing) * sizeof(registro)), SEEK_SET);
		if(r.chave == chave){
			med = 1;
			return;
			}
		else{
			while(r.chave != -1){
				i++;
				med++;
				hash = h3(chave,i);	
				rewind(arquivo);
				fseek(arquivo, ((hash) * sizeof(registro)), SEEK_SET);
				fread(&r, sizeof(registro), 1,arquivo);
				fseek(arquivo, ((hash) * sizeof(registro)), SEEK_SET);
				if(r.chave == chave){
					med++;
					return;
									}			
			} // fim-while	
			
		} // fim-else
}
 
// -----------------------------------MEDIA TOTAL---------------------------------------------------------------- 
// ---------------------------------------------------------------------------------------------------------------  
 
float mediaTotal(){
		registro r;
		int i=0,soma=0;
		float ctd=0;
		
		arquivo = fopen(Arquivo , "rb+");
		rewind(arquivo);
		for(i=0;i<TAMANHO_ARQUIVO;i++){
			fseek(arquivo, (i * sizeof(registro)), SEEK_SET);
			fread(&r, sizeof(registro), 1,arquivo);
			fseek(arquivo, (i * sizeof(registro)), SEEK_SET);
			if(r.chave != -1){
				int ch = r.chave;
				MediaIndividual(ch);
				soma += med;
				ctd++;
			}	
			} //fim-for
		fclose(arquivo);
		if(ctd == 0)
			return 0.0;
		return soma / ctd;	
 }
 
// --------------------------- REMOVE----------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------- 
 
void Remove(int chave){
		registro r;
		int hash,i=0;
		
		 if(chave == -1)
				return;
		
		int hashing = h3(chave,i);
		arquivo = fopen(Arquivo,"rb+");
		rewind(arquivo);
		fseek(arquivo, ((hashing) * sizeof(registro)), SEEK_SET);
		fread(&r, sizeof(registro), 1,arquivo);
		fseek(arquivo, ((hashing) * sizeof(registro)), SEEK_SET);
		if(r.chave == chave){
			r.chave = -1;
			fseek(arquivo, ((hashing) * sizeof(registro)), SEEK_SET);
			fwrite(&r, sizeof(registro), 1, arquivo); 
			fclose(arquivo);
			return;
			}
		else{
			while(r.chave != -1){
				i++;
				hash = h3(chave,i);	
				rewind(arquivo);
				fseek(arquivo, ((hash) * sizeof(registro)), SEEK_SET);
				fread(&r, sizeof(registro), 1,arquivo);
				fseek(arquivo, ((hash) * sizeof(registro)), SEEK_SET);
				if(r.chave == chave){
					r.chave = -1;
					fseek(arquivo, ((hash) * sizeof(registro)), SEEK_SET);
					fwrite(&r, sizeof(registro), 1, arquivo); 
					fclose(arquivo);
					return;
									}			
			} // fim-while	
			
		} // fim-else
		
		printf("chave nao encontrada: %d\n",chave);
		fclose(arquivo);
	
}
 
 
  
// --------------------------- IMPRIME---------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
  
void Imprime(){ 

    registro r;
    arquivo = fopen(Arquivo, "rb+");
    rewind(arquivo);
    int i;
	

    for(i=0; i<TAMANHO_ARQUIVO; i++ ){
		fseek(arquivo, ((i) * sizeof(registro)), SEEK_SET);
		fread(&r, sizeof(registro), 1, arquivo);
		
		if(r.chave == -1)
			printf("%d: vazio\n",i);
		else
            printf("%d: %d %s %d\n", i, r.chave, r.nome, r.idade);
       				
	}
	fclose(arquivo);
    return;         
 }

//--------------------------MAIN---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------

int main()
{
	char op;
	int chave,idade,c,ctd=0;
	char nome[21];
	media2 = 0.0;
	
	IniciarArquivo();
	while(1){
		scanf("%c",&op);
		
		switch(op){	
			case 'i':  
					  scanf("%d %[^\n] %d", &chave, nome, &idade);
					  Insere(chave,nome,idade);
					  ctd++;
					  break;
						
			case 'p': Imprime();break;	
			case 'c': scanf("%d",&c); consulta(c,0);break;		
			case 'm': printf("%.1f\n",mediaTotal()); break;
			case 'r': scanf("%d",&c); Remove(c);break;
					  
			case 'e': return 0; break;	
			
				}	
	}
	
	return 0;
}
